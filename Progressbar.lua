local component = require("component")
local gpu = component.gpu

local pb = {}

local screenWidth, screenHeight = gpu.getResolution()

--[[@param :
        value : valeur de la progression
        maxValue : 100% de la progression
        x, y : coordonées du coin supérieur gauche
        w, h : largeur et longueur de la barre
        bg, fg : couleur de la barre et de son fond
        dir : TODO
]]
function pb.newProgressbar(value, maxValue, x, y, w, h, bg, fg, dir)
    local tmpTable = {}
    tmpTable["Type"] = "Progressbar"
    tmpTable["Value"] = value
    tmpTable["MaxValue"] = maxValue
    tmpTable["X"] = x
    tmpTable["Y"] = y
    tmpTable["Width"] = w
    tmpTable["Height"] = h
    tmpTable["BGColour"] = bg
    tmpTable["FGColour"] = fg
    tmpTable["direction"] = dir or 0

    return tmpTable
end

--calcule le ratio de progression de la barre
local function updateProgress(progressbar)
    valueWidth = progressbar.value * progressbar.Width
    valueWidth = math.floor(valueWidth/progressbar.maxValue)

    return valueWidth
end

--change la valeur de la barre de progression
function pb.changeValue(newValue, progressbar)
    if(newValue <= progressbar.maxValue)
        progressbar.Value = newValue
end

--affiche la barre de progression
function pb.displayProgressbar(progressbar)

    valueWidth = updateProgress(progressbar)

    gpu.setForeground(0x000000)

    gpu.setBackground(progressbar.BGColour)
    gpu.fill(progressbar.X, progressbar.Y, progressbar.Width, progressbar.Height, " ") -- BG of the bar
    gpu.setBackground(progressbar.FGColour)
    gpu.fill(progressbar.X, progressbar.Y, valueWidth, progressbar.Height, " ") -- actual state of the bar    
    
end

return pb